# etcsoft-parent

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/org.etcsoft/etcsoft-parent/badge.svg)](https://maven-badges.herokuapp.com/maven-central/org.etcsoft/etcsoft-parent)
[![Build Status](https://travis-ci.org/mauroarias/etcsoft-parent.svg?branch=master)](https://travis-ci.org/mauroarias/etcsoft-parent)

This library includes some common libraries & plugins to build microservices. 

## Compiling
```
mvn clean install
```
